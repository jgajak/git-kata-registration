public class Uczestnik  {
    private static int liczbaKobiet = 0;
    private String imie;
    
    Uczestnik(String imie) {
        this.imie = imie;
        if (imie.substring(imie.length() - 1).equals("a")) liczbaKobiet++;
    }
    
    public static int getLiczbaKobiet() { return liczbaKobiet; }
    
    }
}
